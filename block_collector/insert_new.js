const fs = require('fs');
const moment = require('moment');
const async = require("async");
const _ = require("lodash");
var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : '172.40.27.117',
  user     : 'root',
  password : 'hello1234',
  database : 'nodeone'
});

connection.connect();

function insertTrx(block_num, items, callback) {

  async.eachSeries(items, function(item, cb) {
    var trx_sig = _.at(item, 'trx.signatures');
    var item = {
      status: item.status,
      cpu_usage_us: item.cpu_usage_us,
      net_usage_word: item.net_usage_word,
      trx_id: item.trx.id,
      trx_signatures: trx_sig ? trx_sig.join(',') : trx_sig,
      trx_signatures_cnt: trx_sig ? trx_sig.length : 0,
      block_num: block_num,
      raw_json: JSON.stringify(item)
    }
    //console.log(item);
  
    connection.query('INSERT INTO transaction SET ?', item, function (error, results, fields) {
      //if (error) {
      //   console.log(error);
      //}
      cb(error);
    });
  }, function(error) {
     callback(error, true);
  });

}

function insert(row, callback) {

  var item = {
    id: row.id,
    block_num: row.block_num,
    producer: row.producer,
    previous: row.previous,
    timestamp: row.timestamp,
    transaction_mroot: row.transaction_mroot,
    action_mroot: row.action_mroot,
    schedule_version: row.schedule_version,
    producer_signature: row.producer_signature,
    ref_block_prefix: row.ref_block_prefix,
    num_of_trx: row.transactions.length
  }

  connection.query('INSERT INTO block SET ?', item, function (error, results, fields) {
    //console.log('Block inserted');
    callback(error, results);
  });

}


var file = process.argv[2];
var text = fs.readFileSync(file, 'utf8');
var j = JSON.parse(text);

async.waterfall([
  function block(callback) {
    //console.log('Inserting block');
    insert(j, callback);
  },
  function voterHook(result, callback) {
    callback(null, true);
  },
  function proxyHook(result, callback) {
    callback(null, true);
  },
  function trx(result, callback) {
    //console.log(result, 'result');
    //console.log('Inserting transactions');
    insertTrx(j.block_num, j.transactions, callback);
  }
], function(error, result) {
  if (error) {
    console.log(error);
  } else {
    console.log('success');
  }
  connection.end();
  process.exit();
});

#!/bin/bash

cd /home/ubuntu/tools/block_collector

NODE=/home/ubuntu/.nvm/versions/node/v8.11.3/bin/node


# Get local head block number
local_head=`$NODE local.js`
echo $local_head

# Get origin head block number
origin_head=`../cleos.sh get info | jq '.head_block_num'`
echo $origin_head

# Get status
status=`cat status.txt`

if [ "$local_head" -eq "$origin_head" ];
then
  echo "local head is up to date"
  exit 1
fi

if [ "$status" -eq "1" ];
then
  echo "running already"
  exit 1
fi

echo "1" > status.txt

i=$(($local_head + 1))
while [ "$i" -lt "$origin_head" ]; 
do

  echo "Fetching Block #$i"
  
  file="block.$i.txt"
  block=`../cleos.sh get block $i > $file`

  result=`$NODE insert.js $file 2> error.txt`

  if [ "$result" != "success" ];
  then
    echo "failed at #$i"
    ./sendmail.sh "failed at #$i"
    exit 1
  fi 
  rm $file

  i=$(($i + 1))
  
done

echo "0" > status.txt

echo "end"
#./sendmail.sh "finished by $origin_head"

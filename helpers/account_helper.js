var exec = require('child_process').exec,
    child;

function get(account_name, callback) {
  child = exec(`/home/ubuntu/tools/cleos.sh get account ${account_name} -j`, function (error, stdout, stderr) {
    //console.log('stdout: ' + stdout);
    //console.log('stderr: ' + stderr);
    if (error !== null) {
        console.log('exec error: ' + error);
    }
    callback(error, stdout); 
  });
}

module.exports = {
  get: get
};

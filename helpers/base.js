
module.exports = {
  db: {
    host     : process.env.DB_HOST,
    user     : process.env.DB_USER,
    password : process.env.DB_PASSWORD,
    db       : process.env.DB_SCHEME,
    charset  : 'utf8'
  },
  create: function(c, sql, cb) {
    var result = false;
    var error = null;

    c.query(sql, function(err, rows) {
      if (err)
        error = err;
      else {
        result = rows.info ? rows.info.insertId : null;  
      }
      
    });

    c.on('end', function() {
      if (error) {
        console.error(error, sql);
      }

      if (cb)
        cb(error, result);
    });

    c.end();
  },
  update: function(c, sql, cb) {
    var result = false;
    var error = null;
    console.log(sql);
    c.query(sql, function(err, rows) {
      if (err)
        error = err;
      else {
        result = rows.info;  
      }
    });

    c.on('end', function() {
      if (error) {
        console.error(error, sql);
      }

      if (cb)
        cb(error, result);
    });

    c.end();
  },
  get: function(c, sql, cb) {
    var result = false;
    var error = null;
    if ('production' !== process.env.NODE_ENV )
      console.log(sql);
    c.query(sql, function(err, rows) {
      if (err)
        error = err;
      else {
        result = rows;
        // console.log(result);
      }
    });

    c.on('end', function() {
      if (error) {
        console.error(error, sql);
      }
      cb(error, result);
    });

    c.end();
  }
};


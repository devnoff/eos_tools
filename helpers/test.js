const _ = require("lodash");
const VoterModel = ("./voters_model");
const sample = require("./sample.json");
const trxHelper = require("./transaction_helper");
const accountHelper = require("./account_helper");
const voters = trxHelper.getVotersOfBp('eosnodeonebp', sample.transactions);
const proxies = trxHelper.getProxy(sample.transactions);
console.log(voters, 'voters');
console.log(proxies, 'proxies');

for (var v of voters) {
  accountHelper.get(v.voter, function(err, acc) {
    if (!err) {
      acc = JSON.parse(acc);
      var vote = {
        owner: _.get(acc, 'voter_info.owner'),
        producers: _.get(acc, 'voter_info.producers').join(','),
        staked: _.get(acc, 'voter_info.staked'),
        is_proxy: _.get(acc, 'voter_info.is_proxy'), 
        last_trx_id: v.trx_id,
        date_in: 'NOW()'
      };
      console.log(vote);
    }
  });
}


for (var p of proxies) {
  accountHelper.get(p.voter, function(err, acc) {
    if (!err) {
      acc = JSON.parse(acc);
      var proxy = {
        owner: p.voter,
        proxy: p.proxy,
        staked: _.get(acc, 'voter_info.staked'),
        created: 'NOW()'
      };
      console.log(proxy);
    }
  });
}


var file = 'sample.json';
var text = fs.readFileSync(file, 'utf8');
var j = JSON.parse(text);

const voteHook = require("./vote_tracking_hook");

async.waterfall([
  //function block(callback) {
  //  insert(j, callback);
  //},
  function voterHook(callback) {
    const voters = hook.getVotersOfBp('eosnodeonebp', j.transactions);
    if (voters.length > 0) {
      async.eachSeries(voters, function(voter, cb) {
        accountHelper.get(voter, function(err, acc) {
          if (!err) {
            acc = JSON.parse(acc);
            var obj = {
              owner: _.get(acc, 'voter_info.owner'),
              producers: _.get(acc, 'voter_info.producers').join(','),
              staked: _.get(acc, 'voter_info.staked'),
              is_proxy: _.get(acc, 'voter_info.is_proxy'), 
              last_trx_id: v.trx_id,
              date_in: 'NOW()'
            };
            VoterModel.insertVotersOfUs(obj, function(error, insertId) {
              cb(error);
            });
          }
        });
      }, function(err) {
        callback(err, true);
      });
    }
    callback(null, true);
  },
  function proxyHook(result, callback) {
    callback(null, true);
  },
  function trx(result, callback) {
    //console.log(result, 'result');
    //console.log('Inserting transactions');
    insertTrx(j.block_num, j.transactions, callback);
  }
], function(error, result) {
  if (error) {
    console.log(error);
  } else {
    console.log('success');
  }
  connection.end();
  process.exit();
});

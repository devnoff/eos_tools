const _ = require('lodash');

function getVotersOfBp(bpname, trxs) {
  let voters = [];
  for(var t of trxs) {
    const trx = t.trx;
    if (typeof trx == 'object') {
      const actions = _.get(trx, 'transaction.actions');
      const action = _.find(actions, { name: 'voteproducer' });
      const producers = _.get(action, 'data.producers');
      const exist = _.indexOf(producers, bpname);
      if (exist > -1) {
        console.log(`${action.data.voter} voted for us!`);  
        voters.push({
          voter: action.data.voter,
          trx_id: trx.id
        });
      }
    }
  }
  return voters;
}

function getProxy(trxs) {
  let proxies = [];
  for(var t of trxs) {
    const trx = t.trx;
    if (typeof trx == 'object') {
      const actions = _.get(trx, 'transaction.actions');
      let action = _.find(actions, { name: 'voteproducer' });
      let proxy = _.get(action, 'data.proxy');
      if (proxy) {
        proxies.push({
          voter: action.data.voter, 
          proxy: proxy, 
          trx_id: trx.id
        });
      }
    }
  }
  return  proxies;
}


module.exports = {
  getVotersOfBp,
  getProxy
};


const _ = require("lodash");
const async = require("async");
const VoterModel = require("./voter_model");
const trxHelper = require("./transaction_helper");
const accountHelper = require("./account_helper");


function createOrUpdateVotersOfUs(obj, callback) {
  async.waterfall([
    function checkExist(__cb) {
      VoterModel.getVotersOfUsItem(obj.owner, function(error, row) {
        console.log(`Exist on vou :`, row ? 'true' : false);
        __cb(error, row ? true : false);
      });
    },
    function update(exist, __cb) {
      if (!exist) {
        return __cb(null, is_new);
      } 

      delete obj.created;

      VoterModel.updateVotersOfUsItem(obj, function(error, info) {
        if (!error) {
          console.log(info, 'Updated vou item');
        }
        __cb(error, exist);
      });
    },
    function create(exist, __cb) {
      if (exist) {
        return __cb(null, exist);
      }

      VoterModel.insertVotersOfUsItem(obj, function(error, insertId) {
        if (!error) {
          console.log(insertId, 'Created vou item')
        }
        __cb(error, exist);
      });
    }
  ], function(error, exist) {
    callback(error, {is_new: !exist});
  });
}


function createOrUpdateProxy(obj, callback) {
  async.waterfall([
    function checkExist(__cb) {
      VoterModel.getProxy(obj.owner, function(error, row) {
        __cb(error, row ? true : false);
      });
    },
    function update(exist, __cb) {
      if (!exist) {
        return __cb(null, is_new);
      }

      delete obj.created;

      VoterModel.updateProxy(obj, function(error, info) {
				if (!error) {
          console.log(info, 'Updated proxy item');
        }
        __cb(error, exist);
      });
    },
    function create(exist, __cb) {
      if (exist) {
        return __cb(null, true);
      }

      VoterModel.insertProxy(obj, function(error, insertId) {
				if (!error) {
          console.log(insertId, 'Updated proxy item');
        }
        __cb(error, insertId);
      });
    }
  ], function(error, result) {
    callback(error, result);
  });
}



/* Public
 * observer module
 */

function observe(transactions, callback) {

  async.waterfall([
    // NEW VOTERS
    function findNewVotersOfUs(__cb) {
			const voters = trxHelper.getVotersOfBp('eosnodeonebp', transactions);
      if (voters.length < 1) return __cb(null, true);

      let result = {
        created: [],
        updated: []
      }

      async.eachSeries(voters, function(item, cb) {
        const voter = item.voter;
        accountHelper.get(voter, function(err, acc) {
          if (err) return cb(err, null);
          acc = JSON.parse(acc);
          var obj = {
            owner: _.get(acc, 'voter_info.owner'),
            producers: _.get(acc, 'voter_info.producers').join(','),
            staked: _.get(acc, 'voter_info.staked'),
            is_proxy: _.get(acc, 'voter_info.is_proxy'),
            last_trx_id: item.trx_id,
          };
          
          createOrUpdateVotersOfUs(obj, function(error, is_new){
            if (is_new) {
              result.created.push(obj);
            } else {
              result.updated.push(obj);
            }
            cb(error);
          });

        });
      }, function(err) {
        __cb(err, result);
      });
    },
    // TO DO : 빠저나간 Voter 찾기
    // 1. transaction 에서 voters_of_us 에 속한 계정이 voteproducer 를 할 경우
    // producers 목록에 우리 계정이 없는 것을 찾음
    // voters_of_us date_out 에 표시
    // 2. transactions 에서 voters_of_us 에 속한 계정이 unstake를 할 경우 ==> 트랜젝션 확인 필요
    // voters_of_us date_out 에 표시 
    // NOTIFICATION
    function voterChangeNotification(result, __cb){
      if (result.created.length > 0 || result.updated.length > 0) {
        const numeral = require('numeral');
        let msg = result.created.length > 0 ? "<i>New Voters</i>\n" : "";
        var i = 1;
        for (var c of result.created) {
          msg += `${i++}. `;
          msg += `${c.owner} `;
          msg += `${numeral(c.staked / 10000).format('0,0')} EOS \n`;
        }

        msg += result.updated.length > 0 ? "<i>Renewed</i>\n" : "";
        i = 1;
        for (var u of result.updated) {
          msg += `${i++}. `;
          msg += `${u.owner} `;
          msg += `${numeral(u.staked / 10000).format('0,0')} EOS \n`;
        } 
        const request = require('request');
        request.post('http://172.40.27.117:3000/notification', {form:{app_token:'eosnodeone2018', title: 'Voter Notice', content: msg}});
      }
      __cb(null, true);
    },
    // NEW PROXY
    function findNewProxy(passed, __cb) {
      const proxies = trxHelper.getProxy(transactions);
      if (proxies.length < 1) return __cb(null, true);

			async.eachSeries(proxies, function(item, cb) {
        accountHelper.get(item.voter, function(err, acc) {
          if (err) return cb(err, null);

          acc = JSON.parse(acc);
					var proxy = {
						owner: item.voter,
						proxy: item.proxy,
						staked: _.get(acc, 'voter_info.staked')
					};
   	      createOrUpdateProxy(proxy, function(error, success) {
            cb(error);
          });
        });
      }, function(err) {
        __cb(err, true);
      });
    }
  ], function(error, result) {
    callback(error, result);
  });
}

module.exports = {
  observe
};

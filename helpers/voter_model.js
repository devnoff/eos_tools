const Client = require('mariasql');
const base = require('./base');
const squel = require('squel');

function getVotersOfUsItem(owner, callback) {
  var c = new Client(base.db);

  try {
    var sql = squel
      .select()
      .field("*")
      .from("voters_of_us")
      .where("owner like ?", owner)
      .limit(1);

  } catch (e) {
    console.log(owner);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result) {
    callback(error, result[0]);
  }); 
}

function insertVotersOfUsItem(obj, callback) {

  const c = new Client(base.db);
  
  try {
    var sql = squel
      .insert({ replaceSingleQuotes: true })
      .into("voters_of_us")
      .setFields(obj)
      .set('date_in', 'NOW()', {dontQuote: true});
  } catch (e) {
    console.log(obj);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.create(c, sql.toString(), callback);

}

function updateVotersOfUsItem(obj, callback) {
  var c = new Client(base.db);

  try {
    var sql = squel
      .update({ replaceSingleQuotes: true })
      .table("voters_of_us")
      .setFields(obj)
      .set("modified", "NOW()", { dontQuote: true })
      .where("owner like ?", obj.owner);

  } catch (e) {
    if (e) {
      console.log(obj);
      throw e;
    }
  }

  // Query
  base.update(c, sql.toString(), callback);
}



function getProxy(owner, callback) {
  var c = new Client(base.db);

  try { 
    var sql = squel
      .select()
      .field("*")
      .from("proxy")
      .where("owner like ?", owner)
      .limit(1);
      
  } catch (e) {
    console.log(owner);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result) {
    callback(error, result[0]);
  });
}

function insertProxy(obj, callback) {
  const c = new Client(base.db);
  
  try {
    var sql = squel
      .insert({ replaceSingleQuotes: true })
      .into("proxy")
      .setFields(obj);
  } catch (e) {
    console.log(obj);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.create(c, sql.toString(), callback);
}

function updateProxy(obj, callback) {
  var c = new Client(base.db);

  try {
    var sql = squel
      .update({ replaceSingleQuotes: true })
      .table("proxy")
      .setFields(obj)
      .set("modified", "NOW()", { dontQuote: true })
      .where("owner like ?", obj.owner);

  } catch (e) {
    if (e) {
      console.log(obj);
      throw e;
    }
  }

  // Query
  base.update(c, sql.toString(), callback);

}


module.exports = {
  getVotersOfUsItem,
  insertVotersOfUsItem,
  updateVotersOfUsItem,
  getProxy,
  insertProxy,
  updateProxy
};

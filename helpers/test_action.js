const fs = require('fs');
const sample = require("./sample.json");
const Observer = require("./action_observer_hook");

var file = './sample.json';
var text = fs.readFileSync(file, 'utf8');
var j = JSON.parse(text);

Observer.observe(j.transactions, function(error, result) {
  console.error(error);
  console.log(result);
});

